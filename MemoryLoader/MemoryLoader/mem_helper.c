/*
 * mem_helper.c
 *
 * Created: 3/24/2019 11:31:55 PM
 *  Author: vtrre
 */ 
#ifndef F_CPU
#define F_CPU 16000000UL
#endif // F_CPU

#include <stdio.h>
#include <string.h>
#include <util/delay.h>

#include "i2c.h"
#include "mem_control.h"
#include "mem_helper.h"


void write_byte(uint16_t address, char data)
{
	
	uint8_t address_msb = (uint8_t) (address >> 8);
	uint8_t address_lsb = (uint8_t) (address & 0xFF);
	

	if ((address_msb & CONFIG_MASK) != 0)
	{
		printf("CANNOT WRITE TO CONFIG\r\n");
		return;
	}

	connect_i2c(MEM_SLAVE_ADDR, I2C_WRITE);
	send_i2c(address_msb);
	send_i2c(address_lsb);
	send_i2c(data);
	stop_i2c();	
}

void write_word(uint16_t address, const char *word, int write_len)
{
	uint8_t address_msb = (uint8_t) (address >> 8);
	uint8_t address_lsb = (uint8_t) (address & 0xFF);

	if ((address_msb & CONFIG_MASK) != 0)
	{
		printf("CANNOT WRITE TO CONFIG\r\n");
		return;
	}
	if(write_len > 32)
	{
		printf("WriteLen value %d is too large, should be 32 or less\r\n", write_len);
		return;
	}
	if(strlen(word) > 32)
	{
		printf("ADDR 0x%04x Word len %d is too large, should be 32 or less\r\n", address, strlen(word));	
		return;
	}
	if (address >= MAX_WORD)
	{
		printf("VAL 0x%04x is past the max address of 0x%04x", address, MAX_WORD);
		return;
	}
	
	connect_i2c(MEM_SLAVE_ADDR, I2C_WRITE);
	send_i2c(address_msb);
	send_i2c(address_lsb);
	for (int i=0; i< write_len; i++)
	{
		send_i2c(*word++);
	}
	stop_i2c();	
}

void print_config(void)
{
	char byte1 = -1;
	char byte2 = -1;
	
	connect_i2c(MEM_SLAVE_ADDR, I2C_WRITE);
	send_i2c(CONFIG_REG);
	send_i2c(0x00);	
	stop_i2c();
	connect_i2c(MEM_SLAVE_ADDR, I2C_READ);
	byte1 = readAck_i2c();
	byte2 = read_i2c();
	stop_i2c();
	
	printf("*** 24CW128X MEM CHIP CONFIG STATUS ***\r\n");
	printf("\tWPR VALUE: %02x\r\n", byte1);
	printf("\tHAR VALUE: %02x\r\n", byte2);
	return;
}

uint8_t read_byte(uint16_t address)
{
	uint8_t address_msb = (uint8_t) (address >> 8);
	uint8_t address_lsb = (uint8_t) (address & 0xFF);
	
	int8_t retval = -1;
	connect_i2c(MEM_SLAVE_ADDR, I2C_WRITE);
	send_i2c(address_msb);
	send_i2c(address_lsb);
	stop_i2c();
	connect_i2c(MEM_SLAVE_ADDR, I2C_READ);
	retval = read_i2c();
	stop_i2c();	
	return retval;
}

void read_word(uint16_t address, char *dstArray, int dstLen)
{
	uint8_t address_msb = (uint8_t) (address >> 8);
	uint8_t address_lsb = (uint8_t) (address & 0xFF);

	if (dstArray == NULL)
	{
		return;
	}
	connect_i2c(MEM_SLAVE_ADDR, I2C_WRITE);
	send_i2c(address_msb);
	send_i2c(address_lsb);
	stop_i2c();
	connect_i2c(MEM_SLAVE_ADDR, I2C_READ);
	for (int i=0; i<dstLen-1; i++)
	{
		dstArray[i] = readAck_i2c();	
	}
	dstArray[dstLen-1] = read_i2c();	
	stop_i2c();
	return;
}

void print_word(uint16_t address)
{
	char retData[32] = {0};

	read_word(address, retData, 32);
	for (int i=0;i<32;i++)
	{
        if ((i % 16) == 0) 
		{
			if (i != 0)
			{
				printf("\r\n");
			} 
	        printf ("  0x%04x ", address + i);
        }
		printf (" %02x", retData[i]);
	}
	printf("\r\n");
}