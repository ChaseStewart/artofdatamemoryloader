/*
 * mem_control.c
 *
 * Created: 11/25/2018 11:18:06 PM
 * Author : Chase E Stewart
 *
 * 
 */ 

#ifndef __MEM_CONTROL_H
#define __MEM_CONTROL_H

/* Slave information */
#define GENERAL_CALL      0b00000000
#define MEM_SLAVE_ADDR    0b01010000
#define I2C_READ     1
#define I2C_WRITE    0 

/* memory map information */
#define MEM_MAP_START   0x0000
#define MEM_MAP_STOP    0x3FFF
#define WORD_SIZE       0x1F
#define FRAMES_PER_WORD 6
#define FRAME1_START    0x1F
#define FRAME1_STOP     0x6F
#define FRAME2_START    0x7F
#define FRAME2_STOP     0xDF
#define CONFIG_REG		0x80

/*mem offsets*/
#define MEM_OFST_0 0x0000
#define MEM_OFST_1 0x06c0
#define MEM_OFST_2 0x0e40
#define MEM_OFST_3 0x15c0
#define MEM_OFST_4 0x1d40
#define MEM_OFST_5 0x24c0
#define MEM_OFST_6 0x2c40
#define MEM_OFST_7 0x33c0

#define START_OUR_DATA 0x0000
#define END_OUR_DATA   0x3b40

/* Masks */
#define WORD_POS_MASK   0x001F
#define WORD_NEG_MASK   0xFFE0
#define CONFIG_MASK     0x80 
#define MAX_WORD        0x3FE0
#endif // __MEM_CONTROL_H
        