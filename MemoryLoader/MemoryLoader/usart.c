/*
 * usart.c
 *
 * Created: 11/25/2018 11:18:06 PM
 * Author : mrobbins@mit.edu
 *
 * relevant part of mrobbins' attribution note is below 
 */ 

// uart.c
// mrobbins@mit.edu
// Note, this references serial 1, not 0, since the micro does not have 0.

#include <stdint-gcc.h>
#include <stdio.h>


#include "usart.h"
#include <avr/io.h>

void uart_init(void) {
	// set baud rate
	UBRR1H = 0;
	UBRR1L = 103;  // for 9600bps with 16MHz clock and no divide by 8
	
	// enable uart RX and TX
	UCSR1B = (1<<RXEN1)|(1<<TXEN1);
	// set 8N1 frame format
	UCSR1C = (1<<UCSZ11)|(1<<UCSZ10);
	
	// set up STDIO handlers so you can use printf, etc
	fdevopen(&uart_putchar, &uart_getchar);
}

void uart_write(char x) {
	// wait for empty receive buffer
	while ((UCSR1A & (1<<UDRE1))==0);
	// send
	UDR1 = x;
}

uint8_t uart_char_is_waiting(void) {
	// returns 1 if a character is waiting
	// returns 0 if not
	return (UCSR1A & (1<<RXC1));
}

char uart_read(void) {
	// wait
	while(!uart_char_is_waiting());
	char x = UDR1;
	return x;
}

int uart_putchar(char c, FILE *stream) {
	uart_write(c);
	return 0;
}

int uart_getchar(FILE *stream) {
	int x = uart_read();
	return x;
}