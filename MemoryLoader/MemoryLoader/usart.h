/*
 * usart.c
 *
 * Created: 11/25/2018 11:18:06 PM
 * Author : mrobbins@mit.edu
 *
 * NOTE: Please set the fuses as such:
 * avrdude -c usbtiny -p m32u4 -P usb -U lfuse:w:0xde:m -U hfuse:w:0x99:m -U efuse:w:0xf3:m
 * 
 */ 
#ifndef __USART_H
#define __USART_H

#include <stdint-gcc.h>
#include <stdio.h>

FILE mystream;

void uart_init(void);
void uart_write(char x);
uint8_t uart_char_is_waiting(void);
char uart_read(void);

int uart_putchar(char x, FILE *stream);
int uart_getchar(FILE *stream);

#endif // __USART_H