/*
 * main.c
 *
 * Created: 11/25/2018 11:18:06 PM
 * Author : Chase E Stewart
 *
 * NOTE: Please set the fuses as such:
 * avrdude -c usbtiny -p m32u4 -P usb -U lfuse:w:0xde:m -U hfuse:w:0x99:m -U efuse:w:0xf3:m
 * 
 */ 

#define F_CPU 16000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "i2c.h"
#include "usart.h"
#include "mem_control.h"
#include "mem_helper.h"
#include "config_defines.h"

#define START_MEM_ADDR MEM_OFST_0

#define ANIM_START_FRAME 0
#define ANIM_STOP_FRAME 10
#define ANIM_NUM_FRAMES 10
#define LED_ARRAY_LEN 16

const uint8_t anim_a_us36_n[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1},{7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 1, 1, 0, 0},{47, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 1, 0},{47, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 1, 0},{7, 7, 7, 7, 1, 7, 7, 1, 1, 7, 7, 7, 7, 7, 1, 0},{47, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 1},{7, 7, 1, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 1, 0},{47, 7, 1, 1, 1, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 0},{47, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 1, 1, 1, 0}};
const uint8_t anim_a_broadway_n[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1},{1, 1, 1, 1, 1, 1, 1, 7, 7, 7, 7, 7, 7, 47, 47},{1, 1, 1, 1, 7, 7, 1, 47, 47, 7, 7, 7, 7, 47, 47},{1, 7, 7, 7, 7, 7, 1, 47, 47, 7, 7, 47, 47, 47, 47},{7, 1, 1, 1, 7, 7, 1, 47, 47, 47, 47, 47, 7, 47, 47},{7, 1, 1, 1, 1, 1, 0, 7, 7, 7, 7, 47, 7, 7, 47},{1, 1, 1, 1, 1, 1, 0, 7, 7, 7, 7, 47, 7, 7, 47},{7, 7, 7, 7, 7, 7, 0, 7, 7, 7, 7, 1, 7, 47, 47},{1, 1, 7, 7, 7, 7, 1, 7, 7, 7, 7, 1, 7, 7, 7}};
const uint8_t anim_a_iris[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0},{7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0},{47, 47, 47, 7, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0},{47, 47, 47, 7, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0},{47, 47, 47, 7, 7, 7, 7, 7, 7, 1, 1, 1, 0, 0, 0, 0},{7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 0, 0, 0, 0},{7, 7, 7, 1, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0},{7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 7, 1, 0, 0, 0, 0},{7, 7, 7, 1, 1, 7, 7, 7, 7, 1, 1, 1, 0, 0, 0, 0}};
const uint8_t anim_b_pearl[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 7, 7, 7, 47, 7, 7, 47, 161, 161, 47, 47, 47, 47},{1, 1, 7, 7, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47},{1, 1, 7, 7, 7, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47},{1, 1, 7, 7, 7, 47, 7, 7, 7, 47, 47, 47, 47, 47, 47},{1, 1, 7, 7, 7, 47, 47, 47, 7, 47, 47, 7, 47, 47, 47},{7, 1, 7, 7, 7, 47, 7, 7, 7, 47, 47, 7, 47, 47, 47},{1, 1, 7, 7, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47},{1, 1, 7, 7, 7, 47, 47, 47, 47, 47, 47, 7, 47, 47, 7}};
const uint8_t anim_b_flagstaff[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{0, 1, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{7, 7, 161, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{7, 7, 161, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{1, 7, 161, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{47, 7, 161, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{7, 7, 161, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{1, 7, 161, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{1, 7, 161, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{1, 7, 161, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
const uint8_t anim_b_canyon[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{7, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},{7, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},{47, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},{47, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},{47, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},{47, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},{7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},{7, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
const uint8_t anim_c_us36_s[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1},{1, 1, 1, 1, 1, 7, 7, 7, 7, 7, 47, 47, 47, 47, 47, 7},{7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 47, 47, 47, 47, 47, 47},{47, 47, 7, 7, 7, 7, 7, 7, 7, 7, 47, 47, 161, 47, 47, 47},{47, 47, 7, 1, 1, 1, 1, 7, 7, 7, 47, 47, 161, 47, 47, 7},{47, 7, 1, 1, 1, 7, 7, 7, 7, 7, 47, 47, 47, 47, 47, 47},{47, 47, 7, 1, 1, 1, 1, 7, 7, 7, 7, 7, 47, 47, 7, 7},{47, 7, 7, 1, 1, 1, 1, 7, 7, 7, 7, 47, 47, 47, 47, 47},{47, 7, 7, 1, 1, 1, 1, 7, 7, 7, 7, 47, 47, 47, 47, 47}};
const uint8_t anim_c_foothills[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1},{0, 1, 1, 0, 1, 1, 7, 7, 47, 7, 7, 7, 7, 1, 1},{0, 1, 1, 1, 1, 1, 7, 7, 7, 7, 7, 7, 7, 1, 1},{0, 1, 1, 1, 1, 1, 7, 47, 47, 7, 7, 7, 47, 7, 7},{0, 1, 1, 0, 1, 1, 7, 1, 7, 7, 7, 7, 7, 1, 1},{0, 1, 1, 1, 1, 1, 7, 7, 47, 7, 7, 7, 7, 7, 7},{0, 1, 1, 1, 1, 1, 7, 7, 47, 1, 1, 1, 7, 1, 1},{0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},{0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
const uint8_t anim_c_arapahoe[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 7, 1, 1, 1, 1},{7, 7, 7, 7, 47, 47, 47, 47, 161, 47, 47, 47, 161, 47, 47, 47},{7, 7, 7, 7, 7, 7, 7, 7, 47, 47, 47, 47, 161, 47, 47, 47},{7, 7, 47, 47, 47, 47, 7, 7, 47, 161, 47, 47, 161, 47, 47, 47},{7, 7, 47, 7, 7, 7, 7, 7, 47, 161, 47, 47, 47, 47, 47, 47},{7, 7, 7, 7, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47},{7, 7, 7, 7, 47, 47, 47, 47, 161, 47, 47, 47, 47, 7, 47, 47},{47, 47, 7, 7, 1, 1, 1, 1, 47, 47, 47, 47, 47, 47, 47, 47},{7, 7, 1, 1, 1, 1, 7, 7, 47, 47, 47, 47, 47, 7, 7, 7}};
const uint8_t anim_d_broadway_s[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1},{1, 1, 1, 7, 7, 7, 7, 7, 7, 47, 47, 161, 161, 47, 47, 47},{47, 47, 47, 47, 7, 7, 7, 7, 7, 47, 7, 161, 161, 47, 47, 47},{7, 7, 7, 7, 7, 7, 7, 7, 7, 47, 47, 47, 47, 47, 47, 47},{7, 7, 7, 7, 7, 7, 7, 7, 7, 47, 7, 47, 47, 47, 47, 47},{7, 7, 7, 7, 7, 7, 7, 7, 7, 47, 7, 47, 47, 47, 47, 47},{7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 47, 47, 47, 47, 47},{7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 47, 47, 47, 47, 47},{7, 7, 7, 7, 7, 7, 7, 7, 7, 1, 7, 7, 7, 7, 7, 7}};
const uint8_t anim_d_baseline[ANIM_NUM_FRAMES][LED_ARRAY_LEN] = {{0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1},{7, 7, 7, 47, 47, 7, 7, 7, 7, 7, 1, 47, 7, 47, 7},{7, 7, 47, 47, 161, 7, 7, 7, 7, 7, 7, 47, 47, 161, 47},{1, 1, 7, 47, 161, 7, 7, 7, 7, 7, 7, 47, 47, 161, 47},{47, 47, 47, 47, 161, 7, 7, 7, 7, 1, 1, 47, 47, 161, 47},{7, 7, 7, 7, 47, 7, 7, 7, 7, 7, 1, 47, 7, 47, 47},{1, 1, 7, 7, 47, 7, 7, 7, 7, 1, 1, 7, 47, 47, 47},{1, 1, 7, 7, 47, 7, 7, 7, 7, 1, 1, 7, 7, 47, 7},{1, 1, 7, 7, 7, 7, 7, 7, 7, 1, 1, 7, 47, 47, 47}};

int main(void)
{
	int frame_id        = 0;
	char word_buffer[33] = {0};
	uint16_t start_address   = START_MEM_ADDR;		
	uint16_t current_address = START_MEM_ADDR;

	DDRF  |= 0x03;

	uart_init();
	init_i2c();
	
	printf("\r\n\n***START***\r\n");
	print_config();
	
	memset(word_buffer, 0, 33);
	
    /* Replace with your application code */
	while(frame_id < ANIM_NUM_FRAMES)
	{
		// WORD 1
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[i] = anim_a_us36_n[frame_id][i];
		}
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[LED_ARRAY_LEN+i] = anim_a_broadway_n[frame_id][i];
		}
		write_word(current_address, word_buffer,32);
		current_address += 0x20;
		_delay_ms(300);
		
		memset(word_buffer, 0, 33);

		// WORD 2
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[i] = anim_a_iris[frame_id][i];
		}
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[LED_ARRAY_LEN+i] = anim_b_pearl[frame_id][i];
		}
		write_word(current_address, word_buffer,32);
		current_address += 0x20;
		_delay_ms(300);

		// WORD 3
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[i] = anim_b_flagstaff[frame_id][i];
		}
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[LED_ARRAY_LEN+i] = anim_b_canyon[frame_id][i];
		}
		write_word(current_address, word_buffer,32);
		current_address += 0x20;
		_delay_ms(300);

		// WORD 4
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[i] = anim_c_us36_s[frame_id][i];
		}
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[LED_ARRAY_LEN+i] = anim_c_foothills[frame_id][i];
		}
		write_word(current_address, word_buffer,32);
		current_address += 0x20;
		_delay_ms(300);
		
		// WORD 5
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[i] = anim_c_arapahoe[frame_id][i];
		}
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[LED_ARRAY_LEN+i] = anim_d_broadway_s[frame_id][i];
		}
		write_word(current_address, word_buffer,32);
		current_address += 0x20;
		_delay_ms(300);
		
		// WORD 6
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[i] = anim_d_baseline[frame_id][i];
		}
		for(int i=0; i< LED_ARRAY_LEN; i++)
		{
			word_buffer[LED_ARRAY_LEN+i] = 0xFF;
		}
		write_word(current_address, word_buffer,32);
		current_address += 0x20;
		_delay_ms(300);
		
		frame_id++;
	}

	// now test 	
	printf("*** MEMORY CONTENTS from 0x%04x - 0x%04x ***\r\n", start_address, current_address);
	for (uint16_t i=START_OUR_DATA; i< END_OUR_DATA; i+=0x20)
	{
		print_word(i);
	}
	printf("*** DONE ***\r\n");
	
}

