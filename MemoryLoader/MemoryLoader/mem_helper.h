/*
 * mem_helper.h
 *
 * Created: 3/24/2019 11:32:18 PM
 *  Author: vtrre
 */ 

#include <stdbool.h>

#ifndef __MEM_HELPER_H
#define __MEM_HELPER_H

void write_byte(uint16_t address, char byte);

void write_word(uint16_t address, const char *word, int write_len); 

uint8_t read_byte(uint16_t address);

void read_word(uint16_t address, char *dstArray, int dstLen);

bool test_word_addr(uint16_t address);

void print_word(uint16_t address);

void print_config(void);

#endif /* __MEM_HELPER_H */